/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rules.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 12:36:36 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 12:36:38 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	check_duplicate(int **matrix, int row, int col, int num)
{
	int	i;

	i = 1;
	while (i <= 4)
	{
		if (matrix[row][i] == num)
			return (0);
		i++;
	}
	i = 1;
	while (i <= 4)
	{
		if (matrix[i][col] == num)
			return (0);
		i++;
	}
	return (1);
}

int	check_row_left(int **matrix, int row)
{
	int	i;
	int	view;
	int	min;

	view = 1;
	min = matrix[row][1];
	i = 2;
	while (i <= 4)
	{
		if (matrix[row][i] == 0)
			return (0);
		if (matrix[row][i] > min)
		{
			min = matrix[row][i];
			view++;
		}
		i++;
	}
	if (view > matrix[row][0])
		return (0);
	return (1);
}

int	check_row_right(int **matrix, int row)
{
	int	i;
	int	view;
	int	min;

	view = 1;
	min = matrix[row][4];
	i = 3;
	while (i >= 1)
	{
		if (matrix[row][i] == 0)
			return (0);
		if (matrix[row][i] > min)
		{
			min = matrix[row][i];
			view++;
		}
		i--;
	}
	if (view > matrix[row][5])
		return (0);
	return (1);
}

int	check_col_up(int **matrix, int col)
{
	int	i;
	int	view;
	int	min;

	view = 1;
	min = matrix[1][col];
	i = 2;
	while (i <= 4)
	{
		if (matrix[i][col] == 0)
			return (0);
		if (matrix[i][col] > min)
		{
			min = matrix[i][col];
			view++;
		}
		i++;
	}
	if (view > matrix[0][col])
		return (0);
	return (1);
}

int	check_col_down(int **matrix, int col)
{
	int	i;
	int	view;
	int	min;

	view = 1;
	min = matrix[4][col];
	i = 3;
	while (i >= 1)
	{
		if (matrix[i][col] == 0)
			return (0);
		if (matrix[i][col] > min)
		{
			min = matrix[i][col];
			view++;
		}
		i--;
	}
	if (view > matrix[5][col])
		return (0);
	return (1);
}
