/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 11:47:20 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 12:37:43 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrix_utils.h"
#include "rules.h"

int	check_view(int **matrix)
{
	int	i;
	int	ret;

	ret = 1;
	i = 1;
	while (i <= 4)
	{
		if (check_col_down(matrix, i) == 0
			|| check_col_up(matrix, i) == 0
			|| check_row_left(matrix, i) == 0
			|| check_row_right(matrix, i) == 0
		)
			ret = 0;
		i++;
	}
	return (ret);
}

int	is_safe(int **matrix, int row, int col, int num)
{
	return (check_duplicate(matrix, row, col, num) == 1);
}

int	solve(int **matrix, int row, int col)
{
	int	num;

	if (check_view(matrix) == 1)
		return (1);
	if (col == 5)
	{
		row++;
		col = 1;
	}
	if (row == 5)
		return (0);
	num = 1;
	while (num <= 4)
	{
		if (is_safe(matrix, row, col, num) == 1)
		{
			matrix[row][col] = num;
			if (solve(matrix, row, col + 1) == 1)
				return (1);
		}
		matrix[row][col] = 0;
		num++;
	}
	return (0);
}
