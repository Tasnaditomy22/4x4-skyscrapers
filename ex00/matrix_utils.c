/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 15:28:44 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 12:08:38 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "helper_functions.h"

void	matrix_initializer(int **matrix, int *input_array)
{
	int	i;
	int	k;

	i = 1;
	k = 0;
	while (k < 4)
	{
		matrix[0][i] = input_array[k];
		matrix[5][i] = input_array[k + 4];
		i++;
		k++;
	}
	i = 1;
	k = 8;
	while (k < 12)
	{
		matrix[i][0] = input_array[k];
		matrix[i][5] = input_array[k + 4];
		i++;
		k++;
	}
}

void	print_matrix_with_borders(int **matrix)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < 6)
	{
		j = 0;
		while (j < 6)
		{
			print_digit(matrix[i][j]);
			print_string(" ");
			j++;
		}
		print_string("\n");
		i++;
	}
}

void	print_solution_matrix(int **matrix)
{
	int	i;
	int	j;

	i = 1;
	j = 1;
	while (i < 5)
	{
		j = 1;
		while (j < 5)
		{
			print_digit(matrix[i][j]);
			print_string(" ");
			j++;
		}
		print_string("\n");
		i++;
	}
}
