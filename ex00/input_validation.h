/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_validation.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 19:55:48 by coder             #+#    #+#             */
/*   Updated: 2021/10/09 20:36:55 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUT_VALIDATION_H
# define INPUT_VALIDATION_H

int	input_string_validation(char *str);
int	argument_processor(char **argv, int *input_array);
int	argument_check(int argc, char **argv, int *input_array);
#endif
