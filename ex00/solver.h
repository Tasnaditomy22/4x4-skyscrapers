/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 11:49:58 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 12:20:11 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SOLVER_H
# define SOLVER_H

int	check_duplicate(int **matrix, int row, int col, int num);
int	check_row_left(int **matrix, int row);
int	check_row_right(int **matrix, int row);
int	check_col_up(int **matrix, int col);
int	check_col_down(int **matrix, int col);
int	check_view(int **matrix);
int	is_safe(int **matrix, int row, int col, int num);
int	solve(int **matrix, int row, int col);
#endif
