/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rules.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 12:36:53 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 12:37:33 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RULES_H
# define RULES_H

int	check_duplicate(int **matrix, int row, int col, int num);
int	check_row_left(int **matrix, int row);
int	check_row_right(int **matrix, int row);
int	check_col_up(int **matrix, int col);
int	check_col_down(int **matrix, int col);
#endif
