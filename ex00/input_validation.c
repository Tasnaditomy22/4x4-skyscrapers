/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_validation.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 13:55:22 by coder             #+#    #+#             */
/*   Updated: 2021/10/09 20:40:44 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "helper_functions.h"

int	input_string_validation(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (i % 2 == 0)
			if (str[i] >= '0' && str[i] <= '4')
				i++;
		else
			return (0);
		else
			if (str[i] != ' ')
				return (0);
		else
			i++;
	}
	return (1);
}

int	argument_processor(char **argv, int *input_array)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (input_string_validation(argv[1]) == 1)
		while (argv[1][i] != '\0' && i < 31)
			if (argv[1][i] == ' ')
				i++;
	else
	{
		input_array[j] = convert_char_to_int(argv[1][i]);
		i++;
		j++;
	}
	else
		return (-1);
	if (i != 31)
		return (-2);
	if (argv[1][i] != '\0')
		return (-3);
	return (0);
}

int	argument_check(int argc, char **argv, int *input_array)
{
	int	check;

	if (argc > 2)
		print_string("Error, too many arguments\n");
	else if (argc < 2)
		print_string("Error, not enough arguments\n");
	else
	{
		check = argument_processor(argv, input_array);
		if (check == -1)
			print_string("Invalid input\n");
		else if (check == -2)
			print_string("Not enough numbers in input string\n");
		else if (check == -3)
			print_string("Input string too long\n");
		else
			return (0);
	}
	return (-1);
}
