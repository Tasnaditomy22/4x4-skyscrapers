/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 12:33:56 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 12:35:31 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "solver.h"
#include "helper_functions.h"
#include "input_validation.h"
#include "matrix_utils.h"
#include "solver.h"
#include <stdlib.h>

int	main(int argc, char **argv)
{
	int	i;
	int	*input_array;
	int	**matrix;
	int	solution;

	input_array = malloc(sizeof(int) * 16);
	matrix = malloc(sizeof(int *) * 6);
	i = 0;
	while (i < 6)
	{
		matrix[i] = malloc(sizeof(int) * 6);
		i++;
	}
	solution = 0;
	if (argument_check(argc, argv, input_array) == 0)
	{
		matrix_initializer(matrix, input_array);
		solution = solve(matrix, 1, 1);
	}
	if (solution == 0)
		print_string("No solution\n");
	else
		print_solution_matrix(matrix);
	return (0);
}
