/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_utils.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 20:00:24 by coder             #+#    #+#             */
/*   Updated: 2021/10/11 11:57:13 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_UTILS_H
# define MATRIX_UTILS_H

void	matrix_initializer(int **matrix, int *input_array);
void	print_matrix_with_borders(int **matrix);
void	print_solution_matrix(int **matrix);
#endif
